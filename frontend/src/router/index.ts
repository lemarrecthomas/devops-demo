import Vue from 'vue';
import Router from 'vue-router';

import RouterComponent from '@/components/RouterComponent.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () => import(/* webpackChunkName: "start" */ '@/views/main/Start.vue'),
      children: [
        {
          path: '',
          component: () => import(/* webpackChunkName: 'home' */ '@/views/Home.vue'),
          name: 'Home',
        },
        {
          path: 'login',
          component: () => import(/* webpackChunkName: "login" */ '@/views/Login.vue'),
          name: 'Login',
        },
        {
          path: 'register',
          component: () => import(/* webpackChunkName: "register" */ '@/views/Register.vue'),
          name: 'Register',
        },
        {
          path: 'recover-password',
          component: () => import(/* webpackChunkName: "recover-password" */ '@/views/PasswordRecovery.vue'),
          name: 'PasswordRecovery',
        },
        {
          path: 'reset-password',
          component: () => import(/* webpackChunkName: "reset-password" */ '@/views/ResetPassword.vue'),
          name: 'ResetPassword',
        },
        {
          path: 'create-password',
          component: () => import(/* webpackChunkName: "create-password" */ '@/views/CreatePassword.vue'),
          name: 'CreatePassword',
        },
      ],
    },
    {
      path: '/*', redirect: '/',
    },
  ],
});
