terraform {
  backend "http" {
  }
}

#########################################################
#                                                       #
#                       PROVIDER                        #
#                                                       #
#########################################################

provider "aws" {
  region  = var.aws_region
}

#########################################################
#                                                       #
#                     DATA SOURCE                       #
#                                                       #
#########################################################

data "aws_iam_policy" "instance_policy" {
  arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

#########################################################
#                                                       #
#                       IAM ROLE                        #
#                                                       #
#########################################################

resource "aws_iam_role" "instance_role" {
  name = "instance_role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "kube_instance_profile" {
  name = "kubernetes_instance_profile"
  role = aws_iam_role.instance_role.name
}

resource "aws_iam_role_policy_attachment" "instance-attach" {
  role       = aws_iam_role.instance_role.name
  policy_arn = data.aws_iam_policy.instance_policy.arn
}

#########################################################
#                                                       #
#                    VPC AND SUBNETS                    #
#                                                       #
#########################################################

#---- VPC ----

resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "VPC"
  }
}

#---- Subnets ----

data "aws_availability_zones" "available" {
  state = "available"
}
resource "aws_subnet" "public1_subnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidrs["public1"]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "public_subnet1"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

resource "aws_subnet" "public2_subnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidrs["public2"]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "public_subnet2"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

#########################################################
#                                                       #
#                  ROUTER AND GATEWAY                   #
#                                                       #
#########################################################

#---- IGW ----

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "IGW"
  }
}

#---- RT ----

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "public_rt"
  }
}

#---- RTA ----

resource "aws_route_table_association" "public1_association" {
  subnet_id      = aws_subnet.public1_subnet.id
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table_association" "public2_association" {
  subnet_id      = aws_subnet.public2_subnet.id
  route_table_id = aws_route_table.public_rt.id
}

#########################################################
#                                                       #
#                   SECURITY GROUP                      #
#                                                       #
#########################################################

#---- Security Group Configuration ----

resource "aws_security_group" "public_security" {
  name        = "public_security"
  description = "Security Group for the Kube Admin"
  vpc_id      = aws_vpc.vpc.id

  tags = {
    "kubernetes.io/cluster/kubernetes" = "owned"
  }

  #---- SSH ----

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.cidrs["personal"], var.cidrs["gitlabrunner"]]
  }

  #---- kubectl logs ----
  ingress {
    from_port   = 1024
    to_port     = 1024
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #---- Postgres port ----
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #---- HTTP Allow ----
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #---- HTTPS Allow ----
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #---- Allow Communication Between Nodes ----
  ingress {
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #---- Allow default security rules ----
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    self        = true
  }

  #---- Allow all ----
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#########################################################
#                                                       #
#                       KEY PAIR                        #
#                                                       #
#########################################################

#---- Key Pair ----

resource "aws_key_pair" "key" {
  key_name   = var.key_name
  public_key = file(var.public_key_path)
}

#########################################################
#                                                       #
#                      INSTANCES                        #
#                                                       #
#########################################################

#---- Kube Master ----

resource "aws_instance" "kube_master" {
  instance_type        = var.kube_instance
  ami                  = var.ami
  iam_instance_profile = aws_iam_instance_profile.kube_instance_profile.id

  tags = {
    Name = "kube_master"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }

  key_name               = aws_key_pair.key.id
  vpc_security_group_ids = [aws_security_group.public_security.id]
  subnet_id              = aws_subnet.public1_subnet.id
}

#---- Kube Workers ----

locals {
  subnet_ids = concat([aws_subnet.public1_subnet.id, aws_subnet.public2_subnet.id])
}

resource "aws_instance" "kube_worker" {
  count                = var.worker_nodes_count
  instance_type        = var.kube_instance
  ami                  = var.ami
  iam_instance_profile = aws_iam_instance_profile.kube_instance_profile.id

  tags = {
    Name                               = "kube_worker ${count.index + 1}"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }

  key_name               = aws_key_pair.key.id
  vpc_security_group_ids = [aws_security_group.public_security.id]
  subnet_id              = element(local.subnet_ids, count.index)

}

#########################################################
#                                                       #
#                        OUTPUT                         #
#                                                       #
#########################################################

output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "public_subnet_id" {
  value = aws_subnet.public1_subnet.id
}

output "kube_sg_id" {
  value = aws_security_group.public_security.id
}

output "key_pair" {
  value = aws_key_pair.key.id
}

# terraform init --backend-config="access_key=$AWS_ACCESS_KEY_ID" --backend-config="secret_key=$AWS_SECRET_ACCESS_KEY"
# teraform validate
# terraform apply -auto-approve
# terraform init -backend-config="address=https://gitlab.com/api/v4/projects/26165883/terraform/state/devops-demo" -backend-config="lock_address=https://gitlab.com/api/v4/projects/26165883/terraform/state/devops-demo/lock" -backend-config="unlock_address=https://gitlab.com/api/v4/projects/26165883/terraform/state/devops-demo/lock" -backend-config="username=lemarrecthomas" -backend-config="password=8662-jzff5aaosSPsxFE" -backend-config="lock_method=POST" -backend-config="unlock_method=DELETE" -backend-config="retry_wait_min=5"