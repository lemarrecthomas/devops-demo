variable "aws_region" {}
variable "vpc_cidr" {}
variable "cidrs" {
  type = map(string)
}
variable "key_name" {}
variable "public_key_path" {}
variable "kube_instance" {}
variable "ami" {}
variable "worker_nodes_count" {}
variable "domain" {}
variable "kops_bucket_name" {}