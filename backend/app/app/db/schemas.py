from typing import List, Optional

from pydantic import BaseModel, EmailStr

class RecipeBase(BaseModel):
    title: str
    description: Optional[str] = None

class RecipeCreate(RecipeBase):
    pass

class Recipe(RecipeBase):
    id: int
    owner_id: int

    class Config:
        orm_mode = True

class UserBase(BaseModel):
    email: EmailStr
    is_active = True

class UserCreate(UserBase):
    email: EmailStr
    password: str

class User(UserBase):
    id: int
    is_active: bool
    recipes: List[Recipe] = []

    class Config:
        orm_mode = True 

class Token(BaseModel):
    access_token: str
    token_type: str

class Msg(BaseModel):
    msg: str