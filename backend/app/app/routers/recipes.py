from fastapi import APIRouter, Depends
from typing import List
from sqlalchemy.orm import Session

from ..db import schemas, crud
from .deps import get_db

router = APIRouter()

@router.get("/recipes/", response_model=List[schemas.Recipe])
def read_recipes(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    recipes = crud.get_recipes(db, skip=skip, limit=limit)
    return recipes