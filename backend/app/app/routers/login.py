from datetime import timedelta
from typing import Any

from fastapi import APIRouter, Depends, HTTPException, Body
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from ..db import crud, schemas
from . import deps
from ..settings import security
from ..settings.config import settings
from ..utils import (
    generate_email_token,
    send_reset_password_email,
    verify_email_token,
    send_new_account_email,
)

router = APIRouter()

@router.post("/login/access-token", response_model=schemas.Token)
def login_access_token(
        db: Session = Depends(deps.get_db), form_data: OAuth2PasswordRequestForm = Depends()
    ):
    """
    OAuth2 compatible token login, get an access token for future requests
    """
    user = crud.authenticate(
        db, email = form_data.username, password=form_data.password
    )
    print(user)
    if not user:
        raise HTTPException(status_code=400, detail="Incorrect email or password")
    elif not crud.is_active(user):
        raise HTTPException(status_code=400, detail="Inactive user")
    access_token_expires = timedelta(minutes = settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    return {
        "access_token": security.create_access_token(
            user.id, expires_delta=access_token_expires
        ),
        "token_type": "bearer",
    }

@router.post("/create-user/", response_model=schemas.User)
def create_user(
        user_in: schemas.UserCreate,
        db: Session = Depends(deps.get_db)):
    """
    Create new user
    """
    user = crud.get_by_email(db, email=user_in.email)
    if user:
        raise HTTPException(status_code=400, detail="Email already registered")
    if settings.EMAILS_ENABLED and user_in.email:
        email_token = generate_email_token(email = user_in.email)
        send_new_account_email(
            email_to = user_in.email, username = user_in.email, password=user_in.password, token = email_token
        )
    return user

@router.post("/set-password/", response_model=schemas.Msg)
def set_password(
        token = Body(...),
        new_password = Body(...),
        db: Session = Depends(deps.get_db)
    ):
    """
    Set Password
    """
    email = verify_email_token(token)
    if not email:
        raise HTTPException(status_code=400, detail="Invalid token")
    user = crud.get_by_email(db, email=email)
    if user:
        raise HTTPException(status_code=404, detail="Email already registered")
    hashed_password = security.get_password_hash(new_password)
    user = crud.create_user(db, obj_in=schemas.UserCreate(email=email, password=hashed_password))
    db.add(user)
    db.commit()
    return{"msg": "User successfully created"}


@router.post("/password-recovery/{email}", response_model=schemas.Msg)
def recover_password(email: str, db: Session = Depends(deps.get_db)):
    """
    Password Recovery
    """
    user = crud.get_by_email(db, email)

    if not user:
        raise HTTPException(
            status_code = 404,
            detail = "The user with this username does not exist in the system.",
        )
    password_reset_token = generate_email_token(email = email)
    send_reset_password_email(
        email_to = user.email, email = email, token = password_reset_token
    )
    return {"msg": "Password recovery email sent"}

@router.post("/reset-password/", response_model=schemas.Msg)
def reset_password(
        token = Body(...),
        new_password = Body(...),
        db: Session = Depends(deps.get_db)
    ):
    """
    Reset Password
    """
    email = verify_email_token(token)
    if not email:
        raise HTTPException(status_code=400, detail="Invalid token")
    user = crud.get_by_email(db, email=email)
    if not user:
        raise HTTPException(
            status_code=404,
            detail="The user with this username does not exist in the system.",
        )
    elif not crud.is_active(user):
        raise HTTPException(status_code=400, detail="Inactive user")
    hashed_password = security.get_password_hash(new_password)
    user.hashed_password = hashed_password
    db.add(user)
    db.commit()
    return {"msg": "Password updated successfully"}