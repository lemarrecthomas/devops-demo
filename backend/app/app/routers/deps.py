from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer

from jose import jwt
from pydantic import ValidationError
from sqlalchemy.orm import Session

from ..db import crud, models, schemas
from ..settings import security
from ..settings.config import settings
from ..db.database import SessionLocal

reusable_oauth2 = OAuth2PasswordBearer(
    tokenUrl="/login/access-token"
)

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally: db.close()

def get_current_user(
        db: Session = Depends(get_db), token: str = Depends(reusable_oauth2)
    ):
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
        )
        token_data = schemas.TokenPayload(**payload)
    except (jwt.JWTError, ValidationError):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Could not validate credentials",
        )
    user = crud.user.get(db, id=token_data.sub)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user


def get_current_active_user(
        current_user: models.User = Depends(get_current_user),
    ):
    if not crud.user.is_active(current_user):
        raise HTTPException(status_code=404, detail="Inactive user")
    return current_user