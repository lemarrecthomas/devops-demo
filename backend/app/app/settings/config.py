import secrets

from typing import List
from pydantic import BaseSettings, EmailStr, AnyHttpUrl

class Settings(BaseSettings):
    PROJECT_NAME: str = "Family Share Recipes"

    ALGORITHM: str = "HS256"
    SECRET_KEY: str = secrets.token_urlsafe(32)
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 8

    SERVER_HOST: AnyHttpUrl = "https://familysharerecipes.com"

    SQLALCHEMY_DATABASE_URL: str = "postgresql://ubuntu:cooking5432@ec2-34-205-174-108.compute-1.amazonaws.com:5432/cookingdb"

    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = ["http://localhost", "http://localhost:8080", "https://familysharerecipes.com", "http://160.86.193.103:8080", "http://192.168.20.37:8080"]

    SMTP_TLS: bool = True
    SMTP_PORT: int = 587
    SMTP_HOST: str = "smtp.sendgrid.net"
    SMTP_USER: str = "apikey"
    SMTP_PASSWORD: str = "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"
    EMAILS_FROM_EMAIL: EmailStr = "info@familysharerecipes.com"
    EMAILS_FROM_NAME: str = "family share recipes"

    EMAILS_ENABLED: bool = True
    EMAILS_TEMPLATES_DIR: str = "/app/app/email-templates"
    EMAIL_RESET_TOKEN_EXPIRE_HOURS: int = 48

    EMAIL_TEST_USER: EmailStr = "lemarrecthomas@gmail.com"

    class Config:
        env_file = ".env"

settings = Settings()