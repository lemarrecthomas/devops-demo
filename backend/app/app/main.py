from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from .routers.api import api_router
from .settings.config import settings

app = FastAPI()

@app.get("/ping/")
def ping():
    return {"ping": "pong!"}

if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

app.include_router(api_router)