#!/usr/bin/python
# create yaml file
import sys

def create_worker_config():
    f = open("/tmp/config.txt", "r")
    data = f.read().split(" ")[2:7:2]
    address = data[0]
    token = data[1]
    sha = data[2]

    sample = """apiVersion: kubeadm.k8s.io/v1beta2
kind: JoinConfiguration
discovery:
  bootstrapToken:
    token: "{1}"
    apiServerEndpoint: "{0}"
    caCertHashes: ["{2}"]
nodeRegistration:
  name: {3}
  kubeletExtraArgs:
    cloud-provider: aws"""
    return sample.format(address, token, sha, sys.argv[1])

if __name__ == "__main__":
    text = create_worker_config()
    with open("workers.conf", "w+") as config:
        config.write(text)