import json, requests


def get_terraform_instances():
    # Start the request session
    s = requests.Session()

    # Get request to the gitlab storage where the terraform state is
    json_dict = s.get("https://gitlab.com/api/v4/projects/26165883/terraform/state/devops-demo?access_token=8662-jzff5aaosSPsxFE").json()

    # Query all the resources
    resources = json_dict["resources"]

    # Query the instance groups (if instances are defined with a 'count' attribute, they will be in the same group)
    instance_groups = [resource for resource in resources if resource["type"] == "aws_instance"]
    return instance_groups

# Return the data to put in the ansible library
def create_ansible_dict(instance_groups):
    lines, group_names, instance_counts = [], [], []
    
    # Get into each group of instances
    for group in instance_groups:
        instances = group["instances"]

        # Setup the group name to match ansible requirements
        group_name = group["name"].replace("kube_", "")
        group_names.append(group_name)

        instance_count = 0

        # Get into each instances of the group
        for instance in instances:
            instance_count += 1
            line = ""

            # Create one line per instance, describing this instance
            line += group_name
            if "index_key" in instance:
                line += str(instance["index_key"]+1)
            line += ' ansible_host="'+instance['attributes']['public_dns']+'" private_dns="'+instance['attributes']['private_dns']+'" ansible_user=ec2-user'
            lines.append(line)
        
        # Instance count to keep count of how many instances are in each group
        instance_counts.append(instance_count)

    count = 0
    text_data = ""

    # Form the final data to put in ansible
    for (idx, group) in enumerate(group_names):
        title = group
        if instance_counts[idx] > 1:
            title += "s"
        
        # Setup the title name to match ansible requirements
        title = "[kube"+title+"]"
        text_data += title + "\n"

        # Setup the lines
        for line in lines[count:instance_counts[idx]+count]:
            count += 1
            text_data += line + "\n"

    return text_data


if __name__ == "__main__":
    instances_groups = get_terraform_instances()
    text = create_ansible_dict(instances_groups)

    # Write in the ansible inventory file
    with open("inventory", "w+") as inventory:
        inventory.write(text)